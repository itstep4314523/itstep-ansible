FROM ubuntu:22.04

RUN apt update -y && \
    apt install -y \ 
    dnsutils \
    inetutils-ping \
    openssh-client \
    git \
    python3-pip

RUN mkdir -p /root/.ssh/ && \
    touch ~/.ssh/known_hosts && \
    chmod 700 ~/.ssh && \
    chmod 600 ~/.ssh/known_hosts && \
    mkdir -p /etc/ansible && \
    ssh-keyscan -H www.365up.cloud >> ~/.ssh/known_hosts && \
    ssh-keyscan -H 95.217.176.40 >> ~/.ssh/known_hosts && \
    ssh-keyscan -H 188.245.60.119 >> ~/.ssh/known_hosts && \
    ssh-keyscan -H 65.109.228.114 >> ~/.ssh/known_hosts && \
    ssh-keyscan -H 65.21.158.60 >> ~/.ssh/known_hosts
    
RUN pip3 install ansible-core==2.15.11 ansible==8.7.0

ADD ansible.cfg /etc/ansible/ansible.cfg